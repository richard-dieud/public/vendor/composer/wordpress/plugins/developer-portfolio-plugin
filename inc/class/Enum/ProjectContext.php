<?php

namespace Rd\Wp\Plugin\DevPortfolio\Enum;

if (!class_exists("Rd\Wp\Plugin\DevPortfolio\Enum\ProjectContext")) {
    class ProjectContext
    {
        // NOTE: Be Aware - Changing properties names will have consequences on langs as keys are used as such
        const Pro = "PROJECT_CONTEXT_PRO";
        const Perso = "PROJECT_CONTEXT_PERSO";
    }
}
