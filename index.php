<?php
/*
    Plugin Name: Developer Portfolio by RD
    Plugin URI: https://store.rdieud.com/dev/wordpress/plugins/developer-portfolio
    Description: Handle Potfolio
*/

namespace Rd\Wp\Plugin\DevPortfolio;

require_once 'vars.php';

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

use Rd\Vendor\Php\Autoloader\Autoloader;

$autoloader = new Autoloader();
$autoloader->register();

$autoloader->add_namespace('Rd\Wp\Plugin\DevPortfolio\\', RD_WPPLG_DEV_PORT_ROOTDIR . "inc/");
$autoloader->add_namespace('Rd\Wp\Plugin\DevPortfolio\\', RD_WPPLG_DEV_PORT_ROOTDIR . "inc/class/");

if (class_exists(Plugin::class)) {
    $instance = new Plugin();

    if (isset($instance)) {
        add_action('init', [$instance, 'onInit']);
        add_action('after_switch_theme', [$instance, 'rewriteFlush']);

        // Registers
        register_activation_hook(__FILE__, [$instance, 'onActivate']);
        register_deactivation_hook(__FILE__, [$instance, 'onDeactivate']);
    }
}
