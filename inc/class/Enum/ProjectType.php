<?php

namespace Rd\Wp\Plugin\DevPortfolio\Enum;

if (!class_exists("Rd\Wp\Plugin\DevPortfolio\Enum\ProjectType")) {
    class ProjectType
    {
        // NOTE: Be Aware - Changing properties names will have consequences on langs as keys are used as such
        const Web = "WEB";
        const WebPersonalSite = "WEB__PERSONAL_WEBSITE";
        const WebBlog = "WEB__BLOG";
        const WebEcommerce = "WEB__ECOMMERCE";
        const WebOther = "WEB__OTHER";

        const Library = "LIBRARY";

        const Mobile = "MOBILE";

        const NodeApp = "App__NODE_JS";

        const PrestashopPlugin = "PROJECT_TYPE_PRESTASHOP_PLUGIN";
        const PrestashopTheme = "PROJECT_TYPE_PRESTASHOP_THEME";
        const WebApp = "PROJECT_TYPE_WEBAPP";


        const WordpressPlugin = "PROJECT_TYPE_WORDPRESS_PLUGIN";
        const WordpressTheme = "PROJECT_TYPE_WORDPRESS_THEME";
    }
}
