<?php

namespace Rd\Wp\Plugin\DevPortfolio\Traits;

if (!trait_exists('Rd\Wp\Plugin\DevPortfolio\Traits\ThemeTrait')) {
    trait ThemeTrait
    {
        public function themeInit()
        {
            $this->applyFrontAssets();
            if (function_exists('add_shortcode')) {
                add_shortcode(RD_WPPLG_DEV_PORT_CPT_PROJECT_SHORTCODE, [$this, 'renderShortCode']);
            }
        }

        protected function applyFrontAssets()
        {
            $this->registerFromAssets();
            $this->activateFrontAssets();
        }

        protected function registerFromAssets()
        {
            // STYLES
            wp_register_style(RD_WPPLG_DEV_PORT_PREFIX . 'assets_theme_main_styles', RD_WPPLG_DEV_PORT_ROOTURL . 'assets/styles/f.css');

            // SCRIPTS
            // wp_deregister_script('jquery');
            // wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.slim.min.js', [], false, true);

            wp_register_script(RD_WPPLG_DEV_PORT_PREFIX . 'assets_theme_main_scripts', RD_WPPLG_DEV_PORT_ROOTURL . 'assets/scripts/f.js', ['jquery']);
        }

        protected function activateFrontAssets()
        {
            // STYLES
            wp_enqueue_style(RD_WPPLG_DEV_PORT_PREFIX . 'assets_theme_main_styles');

            // SCRIPTS
            wp_enqueue_script(RD_WPPLG_DEV_PORT_PREFIX . 'assets_theme_main_scripts');
        }

        public function renderShortCode($args)
        {
            // @todo Over secure this !!!
            $slug = $args['id'];

            $posts = get_posts([
                'name'        => $slug,
                'post_type'   => RD_WPPLG_DEV_PORT_CPT_PROJECT,
                'post_status' => 'publish',
                'numberposts' => 1
            ]);

            $post_meta_data = [];
            $post = null;
            if ($posts && $posts[0] instanceof \WP_Post) {
                $post = $posts[0];
                $post_id = $post->ID;
                $post_metas = get_post_meta($post_id);

                foreach ($post_metas as $post_meta_key => $post_meta_val) {
                    if (strpos($post_meta_key, RD_WPPLG_DEV_PORT_CPT_PROJECT) !== false) {
                        $prefix = RD_WPPLG_DEV_PORT_CPT_PROJECT . '_';
                        $post_meta_key = substr($post_meta_key, mb_strlen($prefix));
                        $post_meta_data[$post_meta_key] = $post_meta_val && is_array($post_meta_val) ? current($post_meta_val) : "";
                    }
                }

                extract($post_meta_data);
            }

            $class_pcard = RD_WPPLG_DEV_PORT_PREFIX . "class_pcard";
            $class_pcard_field = $class_pcard . "_field";

            return "
                <div class='$class_pcard'>
                    <div item-field='title' class='$class_pcard_field'>$post->post_title</div>
                    <div item-field='excerpt' class='$class_pcard_field'>$post->post_excerpt</div>
                    <div item-field='type' class='$class_pcard_field'>$type</div>
                    <div item-field='context' class='$class_pcard_field'>$context</div>
                    <div item-field='city' class='$class_pcard_field'>$city</div>
                    <div item-field='date_started' class='$class_pcard_field'>$date_started</div>
                    <div item-field='date_ended' class='$class_pcard_field'>$date_ended</div>
                    <div item-field='website' class='$class_pcard_field'>$website</div>
                    <div item-field='repository' class='$class_pcard_field'>$repository</div>
                    <div item-field='role' class='$class_pcard_field'>$role</div>
                </div>
            ";
        }
    }
}
