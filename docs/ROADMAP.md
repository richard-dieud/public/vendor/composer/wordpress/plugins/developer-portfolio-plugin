# Priority

- P1: High (MUST)
- P2: Medium (SHOULD)
- P3: Low (COULD)

<br />

# BACKLOG

- [ ] / Task 1 ... {P1}
- [ ] / Task 2 ... {P1}
- [ ] / Task 3 ... {P1}

- [ ] / Task 4 ... {P2}
- [ ] / Task 5 ... {P2}
- [ ] / Task 6 ... {P2}

- [ ] / Task 7 ... {P3}
- [ ] / Task 8 ... {P3}
- [ ] / Task 9 ... {P3}

<br />

# VERSIONS

## VERSION_1

### TODOS

- [ ] / Task 4 ... {P1}
- [ ] / Task 5 ... {P1}
- [ ] / Task 6 ... {P1}

### DONE

- [ ] / Task 1 ... {P1}
- [ ] / Task 2 ... {P1}
- [ ] / Task 3 ... {P1}

## Version 1 : PathFinder

### GOALS

- [ ] Goal 1: This is an important goal
- [ ] Goal 2: This is an important goal
- [ ] Goal 3: This is an important goal
- [ ] Goal 4: This is an important goal

### TODOS

- [ ] / Task 2 ... {P1}
- [ ] / Task 3 ... {P1}
- [ ] / Task 4 ... {P1}

### DONE

- [ ] / Task 1 ... {P1}
