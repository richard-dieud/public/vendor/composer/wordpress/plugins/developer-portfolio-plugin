<?php

namespace Rd\Wp\Plugin\DevPortfolio\Traits;

if (!trait_exists('Rd\Wp\Plugin\DevPortfolio\Traits\AdminTrait')) {
    trait AdminTrait
    {
        public function adminInit()
        {
            $this->applyAdminAssets();
        }

        protected function registerAdminAssets()
        {
            // STYLES
            wp_register_style('rd_wp_plg_dev_portfolio_assets_admin_main_styles', RD_WPPLG_DEV_PORT_ROOTURL . 'assets/styles/a.css');

            // SCRIPTS
            wp_register_script('rd_wp_plg_dev_portfolio_assets_admin_main_scripts', RD_WPPLG_DEV_PORT_ROOTURL . 'assets/scripts/a.js');
        }

        protected function activateAdminAssets()
        {
            // STYLES
            wp_enqueue_style('rd_wp_plg_dev_portfolio_assets_admin_main_styles');

            // SCRIPTS
            wp_enqueue_script('rd_wp_plg_dev_portfolio_assets_admin_main_scripts');
        }

        public function applyAdminAssets()
        {
            $this->registerAdminAssets();
            $this->activateAdminAssets();
        }
    }
}
