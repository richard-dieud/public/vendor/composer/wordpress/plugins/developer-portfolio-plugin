<?php

define('RD_WPPLG_DEV_PORT_ROOTURL', plugin_dir_url(__FILE__));

define('RD_WPPLG_DEV_PORT_PREFIX',  'rd_wpplg_dport_');
define('RD_WPPLG_DEV_PORT_GLOBAL_KEY',  'rd_wpplg_dport');


// ----- PLUGIN PATHS

define('RD_WPPLG_DEV_PORT_ROOTDIR', plugin_dir_path(__FILE__));
define('RD_WPPLG_DEV_PORT_ASSETS_DIR', RD_WPPLG_DEV_PORT_ROOTDIR . 'assets/');
define('RD_WPPLG_DEV_PORT_TEMPLATE_DIR', RD_WPPLG_DEV_PORT_ROOTDIR . 'template/');
define('RD_WPPLG_DEV_PORT_CONFIG_DIR', RD_WPPLG_DEV_PORT_ROOTDIR . 'config/');
define('RD_WPPLG_DEV_PORT_DATA_DIR', RD_WPPLG_DEV_PORT_ROOTDIR . 'data/');

// ----- CUSTOM POST TYPE
define("RD_WPPLG_DEV_PORT_CPT_PROJECT", "rd_wpplg_cpt_project");

// METABOX
define("RD_WPPLG_DEV_PORT_MBX_CPT_PROJECT", RD_WPPLG_DEV_PORT_PREFIX . "mbx_cpt_project");

// POST META
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_TYPE", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_type");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_CITY", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_city");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_CONTENT", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_content");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_EXCERPT", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_excerpt");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_CONTEXT", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_context");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_CLIENT", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_client");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_DATE_STARTED", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_date_started");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_DATE_ENDED", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_date_ended");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_REPO", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_repository");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_WEBSITE", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_website");
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_ROLE", RD_WPPLG_DEV_PORT_CPT_PROJECT . "_role");

// SHORTCODE
define("RD_WPPLG_DEV_PORT_CPT_PROJECT_SHORTCODE", "dport_project");
