<?php

namespace Rd\Wp\Plugin\DevPortfolio;

use Rd\Vendor\Wordpress\Factory\DbFactory;
use Rd\Wp\Plugin\DevPortfolio\MetaBox\ProjectMetaBox;

use Rd\Wp\Plugin\DevPortfolio\Traits\AdminTrait;
use Rd\Wp\Plugin\DevPortfolio\Traits\ThemeTrait;

use Rd\Vendor\Php\Utils\ArrayLib;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists("Rd\Wp\Plugin\DevPortfolio\Plugin")) {
    class Plugin
    {
        use AdminTrait, ThemeTrait;

        private $wpdb;
        private $tableName;
        private $createTableQry;
        private $config;
        private $adminPageOptions;

        public function __construct()
        {
            global $wpdb;
            $this->wpdb = $wpdb;

            $this->checkDeps();

            $this->initConfig();
            $this->initDefaultPreData();
        }

        private function checkDeps()
        {
            if (
                // REQUIRED CLASSES
                !class_exists(DbFactory::class)
                || !class_exists(ProjectMetaBox::class)
                // PHP-VENDOR LIB
            ) {
                throw new \Error('Something went wrong : The plugin has a critical dependency issue');
            }
        }

        private function initConfig()
        {
            $configFile = RD_WPPLG_DEV_PORT_CONFIG_DIR . 'config.json';
            if (file_exists($configFile)) {
                $config = json_decode(file_get_contents($configFile));
                if (empty($GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY])) {
                    $GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY] = (object) [
                        "config" => $config
                    ];
                } else {
                    $GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY]->config =  (object) $config;
                }
            }
        }

        public static function getConfig()
        {
            return $GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY]->config;
        }

        private function initDefaultPreData()
        {
            // TAXONOMIES
            $file = RD_WPPLG_DEV_PORT_DATA_DIR . 'taxonomies.json';
            if (file_exists($file)) {

                $data = json_decode(file_get_contents($file));
                if (empty($GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY])) {
                    $GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY] = (object) [
                        "preData" =>  (object) [
                            "taxonomies" => $data
                        ]
                    ];
                } else {
                    $GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY]->preData =  (object) $data;
                }
            }
        }

        public static function getDefaultPreData()
        {
            return $GLOBALS[RD_WPPLG_DEV_PORT_GLOBAL_KEY]->preData;
        }

        function addCustomObjects()
        {
            register_post_type(
                RD_WPPLG_DEV_PORT_CPT_PROJECT,
                array(
                    'labels'            => array(
                        'name'          => __('Projects'),
                        'singular_name' => __('Project'),
                        'add_new_item'  => __('Add a new project'),
                    ),
                    'public'            => true,
                    'menu_position'     => 3,
                    'menu_icon'         => 'dashicons-hammer',
                    'supports' => [
                        'title', 'tags', 'editor', 'author', 'thumbnail', 'excerpt', 'post-thumbnails'
                    ],
                    'has_archive'       => false,
                    'taxonomies' => array('post_tag'),
                    'show_in_rest' => true,
                )
            );

            // TAXONOMY : PROJECT TYPE
            register_taxonomy(
                RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_TYPE,
                RD_WPPLG_DEV_PORT_CPT_PROJECT,
                array(
                    'labels'            => array(
                        'name'          => __('Project Types'),
                        'singular_name' => __('Project Type'),
                        'add_new_item'  => __('Add a new project'),
                        'parent_item'  => __('Select a parent type'),
                    ),
                    // 'rewrite' => array('slug' => 'project-type'),
                    'hierarchical' => true,
                    'show_in_rest' => true,
                )
            );

            // TAXONOMY : PROJECT CONTEXT
            register_taxonomy(
                RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_CONTEXT,
                RD_WPPLG_DEV_PORT_CPT_PROJECT,
                array(
                    'labels'            => array(
                        'name'          => __('Project Contexts'),
                        'singular_name' => __('Project Context'),
                        'add_new_item'  => __('Add a new project'),
                        'parent_item'  => __('Select a parent context'),
                    ),
                    // 'rewrite' => array('slug' => 'project-type'),
                    'hierarchical' => true,
                    'show_in_rest' => true,
                )
            );

            ProjectMetaBox::register();

            $this->rewriteFlush();
        }

        /* ============ ============ DATA ============ ============ */

        private function addPreData()
        {
            $defaultPreData = self::getDefaultPreData();

            // TAXONOMY : PROJECT TYPE
            if (!empty($defaultPreData) && !empty($defaultPreData->project_type) && is_array($defaultPreData->project_type)) {
                $this->addRecursiveTerms($defaultPreData->project_type, RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_TYPE);
            }

            // TAXONOMY : PROJECT CONTEXT
            if (!empty($defaultPreData) && !empty($defaultPreData->project_context) && is_array($defaultPreData->project_context)) {
                $this->addRecursiveTerms($defaultPreData->project_context, RD_WPPLG_DEV_PORT_CPT_PROJECT_FIELD_CONTEXT);
            }
        }

        private function addRecursiveTerms($terms, $taxo, $parentID = null)
        {
            // Add parent
            foreach ($terms as $term) {
                $parTerm = $this->addRecursiveTerm($term, $taxo, $parentID);
                if (!empty($term->children) && !empty($parTerm) && !empty($parTerm["term_id"])) {
                    $this->addRecursiveTerms($term->children, $taxo, $parTerm["term_id"]);
                }
            }
        }

        private function addRecursiveTerm($term, $taxo, $parentID = null)
        {
            $response = null;

            $name = $term->name;
            $slug = $term->slug;
            if (!term_exists($slug, $taxo)) {
                $response = wp_insert_term($name, $taxo, [
                    "slug" => $slug,
                    "parent" => $parentID
                ]);
            }

            return $response;
        }

        /* ============ ============ HOOK HANDLERS ============ ============ */

        function onInit()
        {
            add_action('wp_enqueue_scripts', [$this, 'themeInit']);

            add_action('admin_menu', [$this, 'adminInit']);

            // Register taxymony
            $this->addCustomObjects();

            add_theme_support('post-thumbnails', array(RD_WPPLG_DEV_PORT_CPT_PROJECT));
        }

        function onActivate()
        {
            $this->addCustomObjects();
            $this->addPreData();
        }

        function  onActiveRollback()
        {
        }

        function onDeactivate()
        {
        }

        function onUninstall()
        {
        }

        function rewriteFlush()
        {
            // Helps not manually refresh "Permalinks" structure by going to "Flushing"
            flush_rewrite_rules();
        }
    }
}
